'use strict'

const mongoose = require('mongoose')
const fs = require('fs')
const path = require('path')
const basename = path.basename(__filename)

mongoose.Promise = global.Promise

const db = {} 

fs.readdirSync(__dirname)
  .filter((file) => {
    return file.indexOf('.') !== 0 && file !== basename && file.slice(-3) === '.js'
  })
  .forEach((file) => {
    require(path.join(__dirname, file))(mongoose)
  })

db.mongoose = mongoose

module.exports = db 