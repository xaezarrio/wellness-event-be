const mongoose = require('mongoose');
const Building = mongoose.model('Buildings');

const getAllPostal = async (req, res) => {
    try {
        let data = await Building.aggregate([{$match:{
                POSTAL: { "$regex": `^${req.params.postal}`}
            }}, {$group:{
                _id: "$POSTAL"
            }}, {$limit:10}]);

        console.log({ "$regex": `^${req.params.postal}`});

        if (!data.length) {
            return res.status(400).json({ success: false, data: [], message: 'Data not found!' });
        }
        return res.status(200).json({ success: true, data });
    } catch (err) {
        return res.status(400).json({ success: false, message: err.message });
    }
}

const getAddressByPostal = async (req, res) => {
    try {
        let data = await Building.find({ POSTAL: req.params.postal})
        
        if (!data.length) {
            return res.status(400).json({ success: false, data: [], message: 'Data not found!' });
        }
        return res.status(200).json({ success: true, data });
    } catch (err) {
        return res.status(400).json({ success: false, message: err.message });
    }
}

module.exports = {
    getAllPostal,
    getAddressByPostal
}