const mongoose = require("mongoose")

const RoleSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        timestamps: true
    }
},
{ 
    timestamps: true
})

module.exports = mongoose.model('Roles', RoleSchema);
