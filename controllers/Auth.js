const mongoose = require("mongoose")
const User = mongoose.model('Users');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const register = async (req, res) => {
    let { username, name, password} = req.body
    const salt = await bcrypt.genSalt()
    password = await bcrypt.hash(password, salt)
    
    let user = new User({
        username,
        name,
        password,
    })

    await user.save()
        .then((result) => {
            return res.status(200).json( {success: true, message: 'Data has been added', result })
        }).catch((err) => {
            return res.status(400).json({ success: false, message: err || "Some error while insert data" })
        })

}

const login = async (req, res) => {
    let { username, password } = req.body

    try {
        let user = await User.findOne({ username }).exec()

        if (!user) return res.status(400).json({ success: false, message: "Username doesnt exist!" })
        
        const match = await bcrypt.compare(password, user.password)
        if(!match) return res.status(400).json({ success: false, message: "Password doesnt match!" })

        const userdata = {
            id: user._id, 
            username: user.username, 
            name: user.name, 
            role: user.role, 
            company: user.company, 
            vendor: user.vendor
        }

        const accessToken = jwt.sign(userdata, process.env.ACCESS_TOKEN,{
            expiresIn: '15m'
        });

        const refreshToken = jwt.sign(userdata, process.env.REFRESH_TOKEN,{
            expiresIn: '1d'
        });

        await user.updateOne({ refreshToken })

        res.cookie('refreshToken', refreshToken,{
            httpOnly: true,
            maxAge: 24 * 60 * 60 * 1000
        });

        return res.status(200).json({ success: true, accessToken })
    } catch (err) {
        return res.status(400).json({ success: false, message: err.message })
    }
}

const refreshToken = async(req, res) => {
    try {
        const refreshToken = req.cookies.refreshToken;
        if(!refreshToken) return res.sendStatus(401);

        const user = await User.findOne({ refreshToken }).exec();
        if(!user) return res.sendStatus(403);

        jwt.verify(refreshToken, process.env.REFRESH_TOKEN, (err, decoded) => {
            if(err) return res.sendStatus(403);
            const userdata = {
                id: user._id, 
                username: user.username, 
                name: user.name, 
                role: user.role, 
                company: user.company, 
                vendor: user.vendor
            }

            const accessToken = jwt.sign(userdata, process.env.ACCESS_TOKEN,{
                expiresIn: '20s'
            });
            res.status(200).json({ success: true, accessToken });
        });
    } catch (error) {
        console.log(error);
    }
}


const logout = async(req, res) => {
    const refreshToken = req.cookies.refreshToken;
    if(!refreshToken) return res.sendStatus(204);

    const user = await User.findOne({ refreshToken }).exec();
    if(!user) return res.sendStatus(204);
    
    await user.updateOne({ refresh_token: null });
    
    res.clearCookie('refreshToken');
    return res.sendStatus(200);
}

module.exports = {
    login,
    register,
    refreshToken,
    logout
}