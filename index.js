const express = require("express")
const dotenv = require("dotenv")
const db = require("./models/index.js")
const router = require("./routes/index.js")
const cors = require('cors');
const cookieParser = require('cookie-parser');
const logger = require('morgan')
dotenv.config();

const app = express()

app.use(cors({ credentials:true, origin: process.env.FRONT_END_URL }));
app.use(logger('dev'))
app.use(cookieParser());
app.use(express.json())

db.mongoose
    .connect(process.env.DB_CONNECTION, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }).then(() => {
        console.log(`Database connected!`)
    }).catch( (err) => {
        console.log(`Cannot connect to the database!`)
    });


app.use('/', router)

app.get('/', (req, res) => { 
    res.send(`Hello, its booking online API, nice too meet you!`)
})

app.all("*", (req, res) =>res.status(404).send("Opss, nothing here."))

app.listen(process.env.PORT, () => console.log(`Server running on port: http://localhost:${process.env.PORT}`));
