const mongoose = require("mongoose")

const BuildingSchema = mongoose.Schema({
    ADDRESS: {
        type: String
    },
    POSTAL: {
        type: String
    },
})

module.exports = mongoose.model('Buildings', BuildingSchema);
