const mongoose = require("mongoose")
const Role = require('./role');
const Company = require('./company');
const Vendor = require('./vendor');

const UserSchema = mongoose.Schema({
    username: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    role: {
        type: mongoose.Schema.Types.ObjectId, 
        ref: Role
    },
    vendor: {
        type: mongoose.Schema.Types.ObjectId, 
        ref: Vendor
    },
    company: {
        type: mongoose.Schema.Types.ObjectId, 
        ref: Company
    },
    refreshToken: {
        type: String,
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date
    }
},
{ 
    timestamps: true
})

module.exports = mongoose.model('Users', UserSchema);
