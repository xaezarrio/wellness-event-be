const mongoose = require('mongoose');
const Event = require('./event');
const User = require('./user');
const Company = require('./company');
const Vendor = require('./vendor');

const BookingSchema = mongoose.Schema({
    event: {
        type: mongoose.Schema.Types.ObjectId, 
        ref: Event,
        required: true
    },
    company: {
        type: mongoose.Schema.Types.ObjectId, 
        ref: Company,
        required: true
    },
    vendor: {
        type: mongoose.Schema.Types.ObjectId, 
        ref: Vendor,
        required: true
    },
    purposedDate: {
        type: Object,
        required: true
    },
    confirmedDate: {
        type: Date,
        default: null
    },
    locationPostal: {
        type: String,
        required: true
    },
    locationAddress: {
        type: String,
        required: true
    },
    status: {
        type: String,
        required: true
    },
    remark: {
        type: String,
        default: null
    },
    createdBy: {
        type: mongoose.Schema.Types.ObjectId, 
        ref: User,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
    }
},
{ 
    timestamps: true
})

module.exports = mongoose.model("Bookings", BookingSchema);