# WELLNESS EVENT API

Backend for WELLNESS EVENT APPS

## AWS
* [13.213.29.230](http://13.213.29.230:3000/)

## Tech Stack

* **Server** : Node, Express
* **Library** : Mongoose, Bcrypt, Express Session, Cookie parser, Dotenv, Cors, jsonwebtoken
* **Database** : MongoDB

## Requirements

* [node.js](https://www.npmjs.com/get-npm)
* [mongodb](https://www.mongodb.com/try/download/community)

## Usage

1. Clone this repo.
2. configure .env with .env.example
3. import BookingOnlineDB.sql to mongodb
4. run `npm install` to install all package.
5. run `npm start` to run app in development mode.
6. open browsers and open `localhost:5000`, it should return **Hello, its booking online API, nice too meet you!**