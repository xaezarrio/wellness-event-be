const express = require("express")
const Auth = require("../controllers/Auth.js")
const router = express.Router()


router.get('/request-token', Auth.refreshToken)
router.post('/login', Auth.login)
router.post('/register', Auth.register)
router.delete('/logout', Auth.logout)

module.exports = router;
