const express = require("express")
const Vendor = require("../controllers/Vendor.js")
const { verifyToken } = require("../middleware/VerifyToken.js");
const router = express.Router()


router.get('/', verifyToken, Vendor.getAllData)
router.get('/:id', verifyToken, Vendor.getOneData)
router.post('/', verifyToken, Vendor.storeData)
router.patch('/:id', verifyToken, Vendor.updateData)
router.delete('/:id', verifyToken, Vendor.deleteData)

module.exports = router;
