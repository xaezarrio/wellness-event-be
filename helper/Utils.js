
const getToken = headers => {
  if (headers && headers.authorization) {
    const token = headers.authorization.split(' ')[1];

    return token;
  } else {
    return null;
  }
}

module.exports = {
  getToken
}