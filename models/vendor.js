const mongoose = require("mongoose")

const VendorSchema = mongoose.Schema({
    name: {
        type: String
    },
    address: {
        type: String
    },
})

module.exports = mongoose.model('Vendors', VendorSchema);
