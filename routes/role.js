const express = require("express")
const Role = require("../controllers/Role.js")
const { verifyToken } = require("../middleware/VerifyToken.js");
const router = express.Router()


router.get('/', verifyToken, Role.getAllData)
router.get('/:id', verifyToken, Role.getOneData)
router.post('/', verifyToken, Role.storeData)
router.patch('/:id', verifyToken, Role.updateData)
router.delete('/:id', verifyToken, Role.deleteData)

module.exports = router;
