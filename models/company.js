const mongoose = require("mongoose")

const CompanySchema = mongoose.Schema({
    name: {
        type: String
    },
    address: {
        type: String
    },
})

module.exports = mongoose.model('Companies', CompanySchema);
