const express = require("express")

// declare route
const authRoute = require("./auth.js")
const userRoute = require("./user.js")
const roleRoute = require("./role.js")
const eventRoute = require("./event.js")
const vendorRoute = require("./vendor.js")
const postalcodeRoute = require("./postalcode.js")
const bookingRoute = require("./booking.js")

const router = express.Router()

// use route
router.use('/auth', authRoute)
router.use('/user', userRoute)
router.use('/role', roleRoute)
router.use('/event', eventRoute)
router.use('/vendor', vendorRoute)
router.use('/postal-code', postalcodeRoute)
router.use('/booking', bookingRoute)

module.exports = router;