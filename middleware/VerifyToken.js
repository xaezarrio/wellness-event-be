const jwt = require("jsonwebtoken");

const verifyToken = (req, res, next) => {
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1];
    if(token == null) return res.sendStatus(401);
    jwt.verify(token, process.env.ACCESS_TOKEN, (err, decoded) => {
        if(err && err.name=="TokenExpiredError") return res.sendStatus(401);
        if(err) return res.sendStatus(403);
        req._id = decoded._id;
        next();
    })
}

module.exports = {
    verifyToken
}