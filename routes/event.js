const express = require("express")
const Event = require("../controllers/Event.js")
const { verifyToken } = require("../middleware/VerifyToken.js");
const router = express.Router()


router.get('/', verifyToken, Event.getAllData)
router.get('/:id', verifyToken, Event.getOneData)
router.post('/', verifyToken, Event.storeData)
router.patch('/:id', verifyToken, Event.updateData)
router.delete('/:id', verifyToken, Event.deleteData)

module.exports = router;
