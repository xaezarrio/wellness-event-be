const mongoose = require("mongoose")
const Booking = mongoose.model('Bookings')
const jwt = require('jsonwebtoken');
const { getToken } = require("../helper/Utils");
const { ROLE } = require('../helper/Constants');

const getAllData = async (req, res) => {
    try {
        const token = getToken(req.headers);

        const filterEvent = req.query.event;

        const userData = jwt.decode(token);

        let where = {};
        if (userData.role===ROLE.HR) {
            where = { createdBy: userData.id };
        } else if (userData.role===ROLE.Vendor) {
            where = { vendor: userData.vendor };
        }

        if (filterEvent) where = {...where, event: filterEvent};

        let data = await Booking.find(where).lean().populate('event', 'name').populate('company', 'name').populate('vendor', 'name').exec();
        
        let totalRow = data.length;

        return res.status(200).json({ success: true, data, totalRow });
    } catch (err) {
        return res.status(400).json({ success: false, message: err.message });
    }
}

const storeData = async (req, res) => {
    try {
        const token = getToken(req.headers);

        const userData = jwt.decode(token);

        req.body.company = userData.company;
        req.body.createdBy = userData.id;
        
        let data = new Booking(req.body).save(function (err) {
            if (err) return res.status(400).json({ success: false, message: err.message });
            // saved!
            return res.status(201).json({ success: true, message: 'Data has been added', data });
        });
    } catch (err) {
        console.log(err)
        return res.status(400).json({ success: false, message: err.message });
    }
}

const getOneData = async (req, res) => {
    try {
        let data = await Booking.findById(req.params.id).populate('event', 'name').populate('company', 'name').populate('vendor', 'name')

        if (!data) {
            return res.status(400).json({ success: false, message: 'Data not found!' })
        }
        return res.status(200).json({ success: true, data })
    } catch (err) {
        return res.status(400).json({ success: false, message: err.message })
    }
}

const updateStatusData = async (req, res) => {
    console.log(req.params.id);
    await Booking.findOneAndUpdate({_id: req.params.id},  req.body, (err) => {
        if (!err) {
            return res.status(200).json({ success: true, message: 'Update status event succeed' })
        } else {
            console.log(err)
            return res.status(400).json({ success: false, message: err })
        }
    })
}

module.exports = {
    getAllData,
    storeData,
    getOneData,
    updateStatusData
}