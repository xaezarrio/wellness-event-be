const express = require("express")
const PostalCode = require("../controllers/PostalCode.js")
const { verifyToken } = require("../middleware/VerifyToken.js");
const router = express.Router()


router.get('/:postal', verifyToken, PostalCode.getAllPostal)
router.get('/address/:postal', verifyToken, PostalCode.getAddressByPostal)

module.exports = router;
