const mongoose = require("mongoose")
const Role = mongoose.model('Roles')
const bcrypt = require('bcrypt');

const getAllData = async (req, res) => {
    try {
        let data = await Role.find().exec()
        
        let totalRow = data.length

        return res.status(200).json({ success: true, data, totalRow })
    } catch (err) {
        return res.status(400).json({ success: false, message: err.message })
    }
}

const getOneData = async (req, res) => {
    try {
        let data = await Role.findById(req.params.id)

        if (!data) {
            return res.status(400).json({ success: false, message: 'Data not found!' })
        }
        return res.status(200).json({ success: true, data })
    } catch (err) {
        return res.status(400).json({ success: false, message: err.message })
    }
}

const storeData = async (req, res) => {
    try {
        let data = await Role(req.body)

        await data.save()

        return res.status(201).json({ success: true, message: 'Data has been added', data })
    } catch (err) {
        console.log(err)
        return res.status(400).json({ success: false, message: err.message })
    }
}

const updateData = async (req, res) => {
    await Role.findOneAndUpdate(req.params.id,  req.body, { new: true }, (err, data) => {
        if (!err) {
            return res.status(200).json({ success: true, message: 'Data has been updated', data })
        } else {
            console.log(err)
            return res.status(400).json({ success: false, message: err })
        }
    })
}

const deleteData = async (req, res) => {
    await Role.findByIdAndRemove(req.params.id, (err, data) => {
        if (!err) {
          return res.status(200).json({ success: true, message: 'Data has been deleted' })
        } else {
          console.log(err)
          return res.status(400).json({ success: false, message: err })
        }
    })
  
}

module.exports = {
    getAllData,
    getOneData,
    storeData,
    updateData,
    deleteData
}