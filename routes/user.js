const express = require("express")
const User = require("../controllers/User.js")
const { verifyToken } = require("../middleware/VerifyToken.js");
const router = express.Router()


router.get('/', verifyToken, User.getAllData)
router.get('/:id', verifyToken, User.getOneData)
router.post('/', User.storeData)
router.patch('/:id', verifyToken, User.updateData)
router.delete('/:id', verifyToken, User.deleteData)

module.exports = router;
