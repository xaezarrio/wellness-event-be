const express = require("express")
const Booking = require("../controllers/Booking.js")
const { verifyToken } = require("../middleware/VerifyToken.js");
const router = express.Router()


router.get('/', verifyToken, Booking.getAllData)
router.get('/:id', verifyToken, Booking.getOneData)
router.post('/', verifyToken, Booking.storeData)
router.patch('/:id', verifyToken, Booking.updateStatusData)

module.exports = router;
